<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

// 绑定index模块
\think\Route::bind('module','index');


/**
 * 封装输出函数
 * auth: Lee E-mail: encircles@163.com
 * @param $data
 */
function P($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}